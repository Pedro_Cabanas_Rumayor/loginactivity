package com.example.loginactivity.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.loginactivity.MainActivity
import com.example.loginactivity.R

class LoginFragment : Fragment {
    //var OnClickListener
    var mainOnClickListener: View.OnClickListener? = null

    //Login View
    var clLogin: ConstraintLayout? = null
    var etEmailLogin: EditText? = null
    var etPasswordLogin: EditText? = null
    var btnRegister: Button? = null
    var btnLogin: Button? = null

    constructor(onClicklisExt: View.OnClickListener) {
        mainOnClickListener = onClicklisExt
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Login view.findViewById
        clLogin = view.findViewById(R.id.clLogin)
        etEmailLogin = view.findViewById(R.id.etEmailLogin)
        etPasswordLogin = view.findViewById(R.id.etPasswordLogin)
        btnRegister = view.findViewById(R.id.btnRegister)
        btnLogin = view.findViewById(R.id.btnLogin)
        //Login OnClickListener
        btnRegister!!.setOnClickListener(mainOnClickListener)
        btnLogin!!.setOnClickListener(mainOnClickListener)
    }
}