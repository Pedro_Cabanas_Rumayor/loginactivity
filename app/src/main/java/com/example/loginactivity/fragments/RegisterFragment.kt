package com.example.loginactivity.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.loginactivity.MainActivity
import com.example.loginactivity.R

class RegisterFragment : Fragment {

    //var OnClickListener
    var mainOnClickListener: View.OnClickListener? = null

    //Register view
    var clRegister: ConstraintLayout? = null
    var etEmailRegister: EditText? = null
    var etPasswordRegister: EditText? = null
    var etConfirmPasswordRegister: EditText? = null
    var btnCancelRegister: Button? = null
    var btnConfirmRegister: Button? = null

    constructor(onClicklisExt: View.OnClickListener?) {
        this.mainOnClickListener = onClicklisExt
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registro, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Register view.findViewById
        clRegister = view.findViewById(R.id.clRegister)
        etEmailRegister = view.findViewById(R.id.etEmailRegister)
        etPasswordRegister = view.findViewById(R.id.etPasswordRegister)
        etConfirmPasswordRegister = view.findViewById(R.id.etConfirmPasswordRegister)
        btnCancelRegister = view.findViewById(R.id.btnCancelRegister)
        btnConfirmRegister = view.findViewById(R.id.btnConfirmRegister)
        //Register OnClickListener
        btnCancelRegister!!.setOnClickListener(mainOnClickListener)
        btnConfirmRegister!!.setOnClickListener(mainOnClickListener)
    }
}