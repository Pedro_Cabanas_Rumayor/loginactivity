package com.example.loginactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentTransaction
import com.example.loginactivity.clases.User
import com.example.loginactivity.fragments.LoginFragment
import com.example.loginactivity.fragments.RegisterFragment

class MainActivity : AppCompatActivity(), View.OnClickListener {
    //Fragments
    var lfLoginFragment: LoginFragment = LoginFragment(this)
    var rfRegisterFragment: RegisterFragment = RegisterFragment(this)

    //Gestor Fragments
    var ffFrameFragments: FrameLayout? = null

    //User Data
    var aUserList = arrayListOf<User>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //Frame Fragments findViewByid
        ffFrameFragments = findViewById(R.id.ffFrameFragments)

        //Fragment transition
        var fragmentTransaction: FragmentTransaction = this.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.ffFrameFragments, lfLoginFragment)
        fragmentTransaction.commit()
    }

    override fun onClick(v: View?) {
        when (v) {
            //onclicks de login fragment
            lfLoginFragment.btnRegister -> {
                goToRegistrerView()
            }
            lfLoginFragment.btnLogin -> {
                login()
            }
            //Oncliks de registro fragment
            rfRegisterFragment.btnCancelRegister -> {
                goToLoginView()
            }
            rfRegisterFragment.btnConfirmRegister -> {
                register()
            }
        }
    }

    fun goToLoginView() { //Transition to login fragment
        var fragmentTransaction: FragmentTransaction = this.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.ffFrameFragments, lfLoginFragment)
        fragmentTransaction.commit()
    }

    fun goToRegistrerView() { //Transition to register fragment
        var fragmentTransaction: FragmentTransaction = this.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.ffFrameFragments, rfRegisterFragment)
        fragmentTransaction.commit()
    }

    fun goToContentActivity() {
        var intent = Intent(this, ContentActivity::class.java)
        startActivity(intent);
        this.finish();
    }

    fun login() {
        if (aUserList.size != 0) {
            for (u in aUserList) {
                if (lfLoginFragment.etEmailLogin!!.text.toString() == u.sEmail && lfLoginFragment.etPasswordLogin!!.text.toString() == u.sPassword) {
                    Toast.makeText(
                            getApplicationContext(),
                            "SUCCESSFUL LOGIN", Toast.LENGTH_SHORT
                    ).show()
                    goToContentActivity()
                    return
                }
            }
            Toast.makeText(getApplicationContext(),
                    "INVALID CREDENCIALS, TRY AGAIN", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(getApplicationContext(),
                    "THERE ARE NO USERS REGISTERED", Toast.LENGTH_SHORT).show()
        }
    }

    fun register() {
        if ((rfRegisterFragment.etEmailRegister!!.text.isNullOrEmpty() || rfRegisterFragment.etPasswordRegister!!.text.isNullOrEmpty() || rfRegisterFragment.etConfirmPasswordRegister!!.text.isNullOrEmpty()) == false) {
            if (rfRegisterFragment.etPasswordRegister!!.text.toString() == rfRegisterFragment.etConfirmPasswordRegister!!.text.toString()) {
                var uUser: User = User(rfRegisterFragment.etEmailRegister!!.text.toString(), rfRegisterFragment.etPasswordRegister!!.text.toString())
                aUserList.add(uUser)

                Toast.makeText(applicationContext, "SUCCESFULLY REGISTERED", Toast.LENGTH_SHORT).show()

                goToLoginView()
            } else {
                Toast.makeText(applicationContext, "PASSWORDS ARE NOT EQUAL", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(applicationContext, "CREDENTIALS CAN´T BE EMPTY", Toast.LENGTH_SHORT).show()
        }
    }
}