package com.example.loginactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast

class ContentActivity : AppCompatActivity(), View.OnClickListener {
    var TAG: String = "ContentActivity"

    //Content Activity View
    var btnClose: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        btnClose = findViewById(R.id.btnClose)
        btnClose!!.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnClose -> {
                this.finish()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        Log.v(TAG, "onPause")
        Toast.makeText(getApplicationContext(),
                "BYE", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        Log.v(TAG, "onResume")
        Toast.makeText(getApplicationContext(),
                "WELCOME!", Toast.LENGTH_SHORT).show()
    }
}